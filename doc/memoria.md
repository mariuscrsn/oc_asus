# OVERCLOCKINg PLACA ASUS EXTREME VI OMEGA
Documentación overclocking placa asus extreme rampage

## CONFIGURACIÓN ESCENARIO FÍSICO
- La placa tiene una conexion de 24 ATX con la PSU que proporciona corriente a la placa.
- 4-pin conector para GPUS adicionales.
- 2X8-pin conectores que alimentan la CPU. Cada conector tiene 4 GNDs y 4 VCC de 12V. Ahora solo hay conectado uno. Hara falta conectar el otro para cuando haga overclocking?.  

## DOCUMENTACIÓN CARACTERÍSTICAS DE LA BIOS

#### Dual Intelligent Processors 5
Combina la función de TPU, EPU, DIGI + Power Control, Fan Xpert y la Aplicación Turbo para impulsar el rendimiento del sistema a su potencial óptimo. Equilibra automáticamente el rendimiento del sistema, el ahorro de energía, los niveles y la configuración del ventilador.

#### TPU (Turbo Process Unit)
Microcontrolador inteligente que permite ajustar manualmente la frecuencia de la CPU, la memoria caché de la CPU, las frecuencias centrales, la frecuencia de DRAM y los voltajes relacionados para una mejor estabilidad del sistema y un aumento del rendimiento. Los cambios no se guardan en la bios. **Para que funcione la CPU ratio en la BIOS debe ser auto.** 
Que es CPU strap ???

#### Turbo Core App
Asignar applicaciones a núcleos por separado 

#### EPU (Energy Process Unit)
Chip de ahorro de energía que modera la carga del sistema en TR. Al establecer una potencia máxima configurada puede aparecer 800MHz en opciones de ahorro de energía avanzadas, pero realmente se utiliza la real. Para una mayor eficiencia energética deshabilitan los controladores de E/S mientras no se utilizan
#### DIGI+ Power Control
Controladores digitales de voltaje VRM y DRAM.
- CPU Power Phase Control --> permite establecer el número de fases utilizadas en la regularización del voltaje
- VRM Switching Frequency --> permite cambiar el espectro de cambio de fases: espectro expandido mejora la estabilidad del sistema
- CPU Power thermal control --> un rango más alto permite mejor OC
- CPU Current capability --> un rango más alto permite un OC más agresivo. Qué es realmente??
- CPU Load-Line Calibration --> permite ajustar el rango de voltaje para controlar la línea de carga de la CPU. Ajuste a un valor alto para el rendimiento del sistema o a un valor bajo para la eficiencia energética.
- Power Duty Control --> ajusta la corriente de cada fase de VRM y las condiciones térmicas de cada componente.

#### Fan Xpert 4 --> Regulación de los ventiladores

## VOLTAJES
- DMI voltage
- CPU Standby voltage
- CPU VCCIO voltage
- PCH Core Voltage
- CPU input voltage
- Transmitter CMD | DQ De-emphasis
- CPU BLCK Slew Rate
- CPU BCLK Frequency Slew Rate
- Hay algunos voltages interesantes en la cache voltage
A partir de Skylake+ se elimina el fivr (Fully Integrated Voltage Regulator) y los sustituye por Digital LDO Regulators

## ACRÓNIMOS
- PCH --> platform controller hub, el hub del pci express
- VRM -->


## PRÓXIMOS TRABAJOS Y DUDAS
- MIRAR VOLTAJES MÁXIMOS Y minimos de la cpu, uno muy alto puede dañar la cpu
- MIRAR CUAL ES EL MEJOR PROGRAMA DE STRESS PARA CPU e instalar uno
- VOLTAJES EN LA CPU? EN TPU--> adaptativo y manual
- Que pasa si se sale del threshold el voltaje por ejemplo??
- El resto de voltajes tienen threshold
- Para que se pueden usar el resto de sensor y el belt que hay al lado de la fuente?
- Qué es la línea de carga de la CPU?
- Leer sobre FIVR --> https://www.researchgate.net/publication/271416878_FIVR_-_Fully_integrated_voltage_regulators_on_4th_generation_IntelR_Core_SoCs
-

## DUDAS REUNIÓN
- Hay que hacerle OC  a la caché?
- Mirar bus del sistema si va a todo
- Power gatting vs clock gatting
- Mirarme 




